# README #

The testing console program for fast wildcard matching.



### What is this repository for? ###

If you want to test and compare the speed of this method to others, this is the repository you want.

### Contribution guidelines ###

If you wish to contribute in making this faster, use this repository and not the other. This is just a small extension method, and its easier to just make changes to the Test, than the solo file.

### Who do I talk to? ###

H.A. Sullivan, just message me. I will explain better on my site how the technique works, and why its faster.